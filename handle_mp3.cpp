#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

struct ID3 {
    string artist;
    string song_name;
    string album;
    string year;

    uint32_t file_size;
    uint32_t tag_size;

    uint16_t id3_version;

    float audio_version;
    uint8_t audio_layer;
    int duration;
};

/**
 * Класс ошибки чтения mp3 файла
 */
class MusicReadException {
private:
    // Поле с сообщением
    string m_error;
public:
    // Стандартный конструктор ошибки
    MusicReadException(string error): m_error(error) {}
    const char* getError() {return m_error.c_str();};
};


class ID3Exception {
private:
    string m_error;
public:
    ID3Exception(string error): m_error(error) {}
    const char* getError() {return m_error.c_str();}
};


// Функция из файла bitrate_values.cpp, которая возвращает массив bitrate-ов
map<string , map<string, map<int, int>>> get_bitrate_dict();

/**
 * Определяем и проверяем на корректность протокол
 * @param stream
 * @param stack
 */
void determine_id3_protocol(fstream &stream, ID3 *stack)
{
    // Тут храним название протокола
    char signature[3];
    // Тут храним номер версии. Он занимает 2 байта
    uint16_t version;
    // Перемещаем указатель в файле в самое начало
    stream.seekg(0, ios::beg);
    // Считываем три байта(именно столько должно занимать название в header в тегэ)
    stream.read((char*)signature, 3);
    if((string)signature != "ID3") {
        throw ID3Exception("Файл не использует протокол ID3");
    }
    // Считываем номер версии
    stream.read((char*)&version, sizeof(uint16_t));
    // Вектор с допустимыми значениями
    vector<uint16_t> correct_versions = {2, 3, 4};
    // Если версия не соответствует допустимым - выкидываем ошибку
    if(find(correct_versions.begin(), correct_versions.end(), version) == correct_versions.end())
        throw ID3Exception("Используется устаревшая версия. Файл не будет обработан");
    stack->id3_version = version;
}

/**
 * Устанавливает значение ID3 тега
 * @param stream ссылка на потом файла
 * @param stack ссылка на структуру, хранящую данные о файле
 */
void set_tag_size(fstream &stream, ID3* stack)
{
    // Хранит размер тега (те штуки, которая содержит фреймы(во фреймах содержится информация, которую мы ищем))
    uint32_t tagsize = 0;
    // Хранит байт данных
    uint8_t tag_buffer;
    // Начинаем читать header тега
    // Информация о размере хранится на 6 байте и занимает 4 байта (причем первый бит каждого байта нулевой)
    stream.seekp(6);
    // Выполняем 4 раза
    for(int j = 0; j < 4; j++) {
        // Считываем байт
        stream.read((char *)&tag_buffer, sizeof(uint8_t));
        // Двигаем результирующий набор на 7 бит слево
        tagsize <<= 7;
        // Добавляем новый байт к результирующему набору
        tagsize += tag_buffer;
    }
    // Возвращаем размер Тэга ID3 (10 байт к заголову)
    stack->tag_size = tagsize + 10;
}


/**
 * Функция получает данные и возвращает их
 * @param stream Поток данных
 * @param start Начальное положение данных
 * @param length Длина данных
 */
string read_frame_data(fstream &stream, int start, uint32_t length) {
    // Устанавливаем стартовую позицию
    stream.seekp(start);
    // Результирующая строка
    string result_string;
    // Хранит текущий символ
    uint8_t cur_char;
    // Символ, с помощью которого проверяем на UTF-8
    uint8_t check_char;
    // Пропускаем начальный байт
    stream.seekg(1, ios::cur);
    // Проверяем количество бит данных
    if(((length - 1) % 2) != 0)
        throw ID3Exception("Символы должны быть в UTF-8");

    // Проверяем, есть ли биты, которые информируют о начале потока основных данных
    uint16_t buf;
    for(int j = 0; j < 2; j++) {
        buf<<=8;
        stream.read((char*)&buf, 1);
    }
    if(buf != 0xFFFE) {
        throw ID3Exception("");
    }
    // Проходим по всем символам
    for(int j = 1; j <= (length - 3); j=j+2) {
        // Получаем основной символ
        stream.read((char*)&cur_char, 1);
        // Получаем следующий за ним символ
        stream.read((char*)&check_char, 1);
        // Если он ненулевой - значит символ в UTF-8
        if(check_char != 0)
            // Поэтому выкидываем исключение
            throw ID3Exception("Приложение не поддерживает UTF-8 кодировку");
        // Добавляем символ в строку
        result_string.push_back((char)cur_char);
    }
    return result_string;
}

/**
 *
 * @param frame_name официальный идентификатор фрейма (в виде стоки)
 * @param data_result значение этого идентификатора
 * @param stack
 */
void save_frames(string frame_name, string data_result, ID3* stack) {
    if(frame_name == "TIT2")
        stack->song_name = data_result;
    if(frame_name == "TALB")
        stack->album = data_result;
    if(frame_name == "TYER")
        stack->year = data_result;
    if(frame_name == "TPE1")
        stack->artist = data_result;
}


/**
 * Функция проходится по фреймам и сохраняет их значения в структуру
 * @param stream
 * @param stack
 */
void read_frames(fstream &stream, ID3* stack)
{
    long tag_size = 0;
    vector<string> frame_types = {"TIT2", "TPE1", "TYER", "TALB"};
    // Проматываем заголовок Тeга
    stream.seekg(10, ios::beg);
    while(true) {
        // Хранит идентификатор фрейма
        char frame_id[4];

        // Промежуточная переменная для хранения байта данных
        uint8_t byte_buffer;

        // Хранит размер фрейма
        uint32_t frame_size = 0;
        stream.read((char *) &frame_id, 4);
        // проходим по байтам и считаем размер фрейма
        for (int j = 0; j <= 3; j++) {
            frame_size <<= 8;
            stream.read((char *) &byte_buffer, 1);
            frame_size += byte_buffer;
        }
        // Пропускаем флаги
        stream.seekg(2, ios::cur);
        // Заголовок закончен

        // Сохраняем текущую позицию
        int cur_position = stream.tellp();
        // Если фреймы закончились - всего доброго
        if((string)frame_id == "")
            break;
        // Если нужный фрейм из списка доступных фреймов найден
        if(find(begin(frame_types), end(frame_types), string(frame_id)) != end(frame_types)) {
            // Сохраняем значение фрейма
            string data_result = read_frame_data(stream, cur_position, frame_size);
            // Сохраняем значение в структуру
            save_frames(frame_id, data_result, stack);
        }
        // Прибавляем заголовок
        tag_size += (frame_size + 10);
        // ставим новую позицию для следующей итерации
        stream.seekp(cur_position + frame_size);
        // Если гг - выходим из цикла
        if(stream.tellp() == -1)
            break;
    }
}

void print_result(ID3* stack) {
    cout << "Имя артиста: " << stack->artist << endl;
    cout << "Название альбома: " << stack->album << endl;
    cout << "Название песни: " << stack->song_name << endl;
    cout << "Год выхода песни: " << stack->year << endl;
    cout << "\nСистемные параметры\n" << endl;
    cout << "Версия ID3 тега: " << stack->id3_version << endl;
    cout << "Layer параметр файла: " << (int)(stack->audio_layer) << endl;
    cout << "MPEG версия: " << stack->audio_version << endl;
    cout << "Продолжительность файла: " << stack->duration << "с" << endl;
    cout << "Размер файла: " << stack->file_size << " байт" << endl;
}

/**
 * Функция определяет технические параметры mp3 файла
 * @param stream ссылка на потом файла
 * @param stack ссылка на структуру, хранящую данные о файле
 * Работает все вот так https://www.codeproject.com/Articles/8295/MPEG-Audio-Frame-Header#VBRHeaders
 */
void determine_technical_params(fstream &stream, ID3 *stack) {
    // временное хранилище байта
    uint8_t buffer;
    // MPEG версия
    string audio_version;
    // Layer mp3 файла
    string layer;

    // Перемещаемся к MPEG фрейму(находится после все теговских фреймов)
    stream.seekp(stack->tag_size + 1);
    stream.read((char*)&buffer, 1);
    // Крутим-вертим, чтобы получить Audio version
    buffer<<=3;
    buffer>>=6;
    // Определяем версию
    switch ((int)buffer) {
        case 0: {
            audio_version = "2.5_version";
            stack->audio_version = 2.5;
            break;
        }
        case 2: {
            audio_version = "2_version";
            stack->audio_version = 2;
            break;
        }
        case 3: {
            audio_version = "1_version";
            stack->audio_version = 1;
            break;
        }
    }
    // Опять считываем тот же байт:
    stream.seekg(-1, ios::cur);
    stream.read((char*)&buffer, 1);
    // Туда-сюда, чтобы получить Layout
    buffer<<=5;
    buffer>>=6;
    // Определяем layer
    switch((int)buffer) {
        case 1: {
            stack->audio_layer = 3;
            layer = "layout_3";
            break;
        }
        case 2: {
            stack->audio_layer = 2;
            layer = "layout_2";
            break;
        }
        case 3: {
            stack->audio_layer = 1;
            layer = "layout_1";
            break;
        }
    }
    // Получаем третий байт из главного заголовка фрейма
    stream.read((char*)&buffer, 1);
    // Получаем bitrate файла
    buffer>>=4;

    // Записываем продолжительность файла (вызываем функцию их файла bitrate_values, которая возвращает словарь)
    stack->duration = (stack->file_size - stack->tag_size) / (get_bitrate_dict()[audio_version][layer][(int)buffer] * 1000) * 8;
}

/**
 * Устанавливаем в структуру размер файла
 * @param stream ссылка на потом файла
 * @param stack ссылка на структуру, хранящую данные о файле
 */
void set_file_size(fstream &stream, ID3* stack) {
    // Смещаем указатель в потоке в конец
    stream.seekg(0, ios::end);
    // Получаем последнюю позицию
    stack->file_size = stream.tellp();
    // Сбрасываем к началу
    stream.seekg(0, ios::beg);
}


/**
 * Основная функция по работе с mp3
 * @param path путь к файлу
 */
void handle_mp3(string path)
{
    ID3 stack;
    ID3 *stack_link = &stack;
    // Поток файла
    fstream file_stream(path, ios::binary | ios::in);
    try {
        // Если файл не открыт - значит не существует
        if (!(file_stream.is_open()))
            throw MusicReadException("Данная музыкальная композиция не была найдена");
        // Устанавливаем размер файла
        set_file_size(file_stream, stack_link);
        // Определяем версию протокола
        determine_id3_protocol(file_stream, stack_link);
        // Устанавливаем размер тега
        set_tag_size(file_stream, stack_link);
        // Считываем фреймы
        read_frames(file_stream, stack_link);
        // Определяем технические характеристики файла
        determine_technical_params(file_stream, stack_link);
        // Выводим результат
        print_result(stack_link);
    } catch(MusicReadException &exception) {
        cout << exception.getError() << endl;
        return;
    } catch (ID3Exception &exception) {
        cout << exception.getError() << endl;
    }
}


#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#define MUSIC_DIRECTORY "music_files"


using namespace std;

int handle_mp3(string file_path);


/**
 * @return char Возвращает разделитель для директорий и файлов
 */
char delimiter_type()
{
    char* ostype = getenv("OSTYPE");
    if (ostype == nullptr)
    {
        // Если ОС - Windows
        ostype = getenv("windir");
        if (ostype != nullptr)
            return '\\';
        // Если не Windows
        return '/';
    }
        // Если на базе Linux
    else return '/';
}

/**
 * @param start_path необработанный путь к файлу, который запускал программу
 * @return string обработанный путь к директории с музыкой, определенной в проекте
 */
string path_to_file(string start_path)
{
    // Создаем поток с помощью строки
    stringstream path_stream(start_path);
    // Создаем динамический массив, хранящий раздробленный путь к файлу
    vector<string> directories;
    // Текущий элемент
    string item;
    // Считываем в item строку пути по разделителю
    while(getline(path_stream, item, delimiter_type()))
    {
        // Добавляем её в массив
        directories.push_back(item);
    }
    // Удаляем два последних пути
    directories.pop_back();
    directories.pop_back();

    // Результирующая строка пути
    string result_string;
    for(auto cur_string: directories)
    {
        result_string += cur_string + delimiter_type();
    }

    // Конвертируем в строку макрос директории музыки
    string music_directory(MUSIC_DIRECTORY);
    // Добавляем директорию в путь
    result_string += music_directory + delimiter_type();
    return result_string;
}

int main(int argc, char* argv[]) {
    // Получаем исполняемый скрипт
    string script_path = argv[0];
    string normal_path = path_to_file(script_path);

    string file_name;
    cout << "Введите имя файла: " << endl;
    getline(cin, file_name, '\n');
    // Начинаем обработку
    handle_mp3(normal_path + file_name);
    return 0;
}
